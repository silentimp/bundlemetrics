const withBundleAnalyzer = require('@zeit/next-bundle-analyzer')

const nextConfig = {
  analyzeBrowser: true,
  bundleAnalyzerConfig: {
    browser: {
      analyzerMode: 'disabled',
      defaultSizes: 'gzip',
      generateStatsFile: true,
      statsFilename: 'client.json',
      logLevel: 'warn',
      statsOptions: {
        assets: false,
        cached: false,
        cachedAssets: false,
        chunks:true,
        chunkGroups: false,
        chunkModules: false,
        chunkOrigins: false,
        chunksSort: 'name',
        entrypoints: false,
        modules: false,
        reasons: false,
      },
    }
  },
  webpack (config) {
    return config
  }
}

module.exports = withBundleAnalyzer(nextConfig)
