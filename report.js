const { createGzip } = require('zlib');
const fs = require('fs');
const BASE_DIR = './.next/';
const report = require(`${BASE_DIR}client.json`);
const { chunks } = report;

function gzipSize (path) {
  return new Promise((resolve, reject) => {
    let size = 0;
    let pipe = fs.createReadStream(path).pipe(createGzip({ level: 9 }));
    pipe.on('error', reject);
    pipe.on('data', buf => {size += buf.length});
    pipe.on('end', () => {resolve(size)});
  })
}

const units = (mb, kb, size) => {
  if (mb > 1) return 'Mb';
  if (kb > 1) return 'Kb';
  return 'bytes';
};

const value = (mb, kb, size) => {
  if (mb > 1) return mb.toFixed(2);
  if (kb > 1 ) return Math.floor(kb);
  return size;
};

function compare( a, b ) {
  if ( a.names < b.names ){
    return -1;
  }
  if ( a.names > b.names ){
    return 1;
  }
  return 0;
}

const generateValue = (mb, kb, size) => `${value(mb, kb, size)}${units(mb, kb, size)}`;

try {
  (async () => {
    const gziped = await Promise.all(chunks.map(async ({names, files}) => {
      const size = await gzipSize(`${BASE_DIR}${files[0]}`);
      return {
        names: names[0].split('/').splice(-1)[0],
        size,
      }  
    }));
    const list = gziped.sort(compare).reduce((collector, chunk) => {
      const { names, size } = chunk;
      const mb = size / (1024*1024);
      const kb = size / (1024);
      collector.set(names, generateValue(mb, kb, size));
      return collector;
    }, new Map());
    let report = [];
    list.forEach((value, key) => {
      report.push(`${key} ${value}`);
    });
    process.stdout.write(report.join('\n'));
    process.exit(0);
  })()
} catch (error) {
  process.exit(1);
}
